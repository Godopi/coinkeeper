import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CategoryUsertModalComponent } from './shared/components/category-upsert-modal/category-usert-modal.component';
import { OperationUsertModalComponent } from './shared/components/operation-upsert-modal/operation-usert-modal.component';

import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
import { AuthService } from './core/services/auth.service';
import { SettingsComponent } from './components/settings/settings.component';
import { LongPressDirective } from './shared/directives/long-press.directive';
import { RequestService } from './core/services/request.service';
import { IncomeCategoryUpsertModalComponent } from './shared/components/income-category-upsert-modal/income-category-upsert-modal.component';
import { AccountsUpsertModalComponent } from './shared/components/accounts-upsert-modal/accounts-upsert-modal.component';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    CategoryUsertModalComponent,
    OperationUsertModalComponent,

    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    SettingsComponent,
    LongPressDirective,
    IncomeCategoryUpsertModalComponent,
    AccountsUpsertModalComponent,
  ],
  imports: [
    // FlexLayoutModule,
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
  ],
  providers: [AuthService, RequestService],
  bootstrap: [AppComponent],
})
export class AppModule {}
