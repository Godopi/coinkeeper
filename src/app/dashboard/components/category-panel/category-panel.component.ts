import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  Account,
  AccountBalance,
  ExpCategory,
  IncCategory,
} from 'src/app/core/entities';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatMenuTrigger } from '@angular/material/menu';
import { expenseIcons, incomeWalletIcons } from 'src/app/core/constants';
import { RequestService } from 'src/app/core/services/request.service';
import { filter, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { CategoryUsertModalComponent } from 'src/app/shared/components/category-upsert-modal/category-usert-modal.component';
import { IncomeCategoryUpsertModalComponent } from 'src/app/shared/components/income-category-upsert-modal/income-category-upsert-modal.component';
import { AccountsUpsertModalComponent } from 'src/app/shared/components/accounts-upsert-modal/accounts-upsert-modal.component';
// const mockCategory: ExpCategory[] = [
//   {
//     id: '1',
//     budget: 1000,
//     name: 'Продукты',
//     deleted: false,
//   },

//   {
//     id: '3',
//     budget: 2000,
//     name: 'Медицина',
//     deleted: false,
//   },
// ];
// const incomeMockCategory: ExpCategory[] = [
//   {
//     id: '2',
//     budget: 20000,
//     name: 'Работа',
//     deleted: false,
//   },
// ];
// const incomeMockAcc: Account[] = [
//   {
//     id: 4,
//     name: 'Кошелек',
//     deleted: false,
//     typeId: 1,
//     considerInTotalBalance: true,
//   },
//   {
//     id: 5,
//     name: 'Карта',
//     deleted: false,
//     typeId: 1,
//     considerInTotalBalance: true,
//   },
// ];
@Component({
  selector: 'app-category-panel',
  templateUrl: './category-panel.component.html',
  styleUrls: ['./category-panel.component.scss'],
})
export class CategoryPanelComponent implements OnInit {
  public expCategories: Observable<ExpCategory[]>;
  public incCategories: Observable<IncCategory[]>;
  public accounts: Observable<Account[]>;

  @Input() public accBalance: AccountBalance | undefined;
  // @Input() public incomeCategories: ExpCategory[] = [];
  // @Input() public accounts: Account[] = [];
  // @Output() public createItem: EventEmitter<any>;
  // @Output() public removeItem: EventEmitter<any>;
  // @Output() public changeItem: EventEmitter<any>;
  // timePeriods = [
  //   'Bronze age',
  //   'Iron age',
  //   'Middle ages',
  //   'Early modern period',
  //   'Long nineteenth century',
  // ];
  // @ViewChild(MatMenuTrigger) public trigger: MatMenuTrigger | undefined;
  // isOpen = false;
  // icons = incomeWalletIcons;
  // iconsCat = expenseIcons;

  constructor(
    private readonly dialog: MatDialog,
    private readonly requestService: RequestService
  ) {
    this.expCategories =
      requestService.getCollection<ExpCategory>('expense_categories');
    this.incCategories =
      requestService.getCollection<IncCategory>('income_categories');
    this.accounts = requestService.getCollection<Account>('accounts');
    // this.categories = mockCategory;
    // this.incomeCategories = incomeMockCategory;
    // this.accounts = incomeMockAcc;
    // this.removeItem = new EventEmitter(false);
    // this.changeItem = new EventEmitter(false);
    // this.createItem = new EventEmitter(false);
  }
  // mouseLongPress(event: any) {
  //   // event.openMenu();
  // }
  // onClickMenu(event: MouseEvent) {
  //   this.isOpen = !this.isOpen;
  // }
  ngOnInit(): void {}
  upsertIncCategory(category?: IncCategory) {
    const dialogRef = this.dialog.open(IncomeCategoryUpsertModalComponent, {
      data: category,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((res) => res !== null && res !== undefined))
      .subscribe((result: IncCategory) => {
        category
          ? this.requestService.update(
              'income_categories',
              category.id!,
              result
            )
          : this.requestService.add('income_categories', result);
      });
  }
  removeIncCategory(id: string) {
    this.requestService.remove('income_categories', id);
  }
  upsertExpCategory(category?: ExpCategory) {
    const dialogRef = this.dialog.open(CategoryUsertModalComponent, {
      data: category,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((res) => res !== null && res !== undefined))
      .subscribe((result: ExpCategory) => {
        category
          ? this.requestService.update(
              'expense_categories',
              category.id!,
              result
            )
          : this.requestService.add('expense_categories', result);
      });
  }
  removeExpCategory(id: string) {
    this.requestService.remove('expense_categories', id);
  }
  upsertAccount(acc?: Account) {
    const dialogRef = this.dialog.open(AccountsUpsertModalComponent, {
      data: acc,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((res) => res !== null && res !== undefined))
      .subscribe((result: Account) => {
        acc
          ? this.requestService.update('accounts', acc.id!, result)
          : this.requestService.add('accounts', result);
      });
  }
  removeAccount(id: string) {
    this.requestService.remove('accounts', id);
  }

  // drop(event: CdkDragDrop<string[]>) {
  //   moveItemInArray(this.timePeriods, event.previousIndex, event.currentIndex);
  // }
}
