import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';
import {
  Account,
  ExpCategory,
  IncCategory,
  Operation,
} from 'src/app/core/entities';
import { DashboardService } from '../../dashboard.service';
import { OperationsDay, OperationsList } from './operations-list.model';

@Component({
  selector: 'app-operations-list',
  templateUrl: './operations-list.component.html',
  styleUrls: ['./operations-list.component.scss'],
})
export class OperationsListComponent implements OnInit {
  // @Input() public operationsList: OperationsDay[] = [];
  @Input() public operationsList: OperationsList | undefined;
  // @Input() public keys: string[] = [];
  public get keys(): string[] {
    if (this.operationsList) {
      return Object.keys(this.operationsList).sort((a, b) => {
        return moment(b).diff(a);
      });
    }
    return [];
  }
  // @Input() public incCategories: IncCategory[] = [];
  // @Input() public expCategories: ExpCategory[] = [];
  // @Input() public accounts: Account[] = [];
  @Input() public categories: any[] = [];

  @Output() public onRemoveOperation: EventEmitter<Operation>;
  @Output() public onChangeOperation: EventEmitter<Operation>;

  constructor(private readonly dashboardService: DashboardService) {
    this.onRemoveOperation = new EventEmitter(false);
    this.onChangeOperation = new EventEmitter(false);
  }

  public ngOnInit(): void {}
  public remove(operation: Operation) {
    this.onRemoveOperation.emit(operation);
  }
  public change(operation: Operation) {
    this.onChangeOperation.emit(operation);
  }
}
