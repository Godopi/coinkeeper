import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  Account,
  ExpCategory,
  IncCategory,
  Operation,
} from 'src/app/core/entities';

@Component({
  selector: 'app-operation-card',
  templateUrl: './operation-card.component.html',
  styleUrls: ['./operation-card.component.scss'],
})
export class OperationCardComponent implements OnInit {
  @Input() public operation: Operation | undefined;
  @Input() public categories: any[] = [];
  @Output() public onRemoveOperation: EventEmitter<Operation>;
  @Output() public onChangeOperation: EventEmitter<Operation>;
  constructor() {
    this.onRemoveOperation = new EventEmitter(false);
    this.onChangeOperation = new EventEmitter(false);
  }
  public findCategory(id: string): Account | IncCategory | ExpCategory {
    return this.categories.find((cat) => cat.id === id);
  }
  public ngOnInit(): void {}
  public remove(operation: Operation) {
    this.onRemoveOperation.emit(operation);
  }
  public change(operation: Operation) {
    this.onChangeOperation.emit(operation);
  }
}
