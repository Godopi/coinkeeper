import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Operation } from 'src/app/core/entities';
import { OperationsDay } from '../operations-list.model';

@Component({
  selector: 'app-day-card',
  templateUrl: './day-card.component.html',
  styleUrls: ['./day-card.component.scss'],
})
export class DayCardComponent implements OnInit {
  @Input() public days: Operation[] | undefined;
  @Input() public categories: any[] = [];
  @Output() public onRemoveOperation: EventEmitter<Operation>;
  @Output() public onChangeOperation: EventEmitter<Operation>;
  constructor() {
    this.onRemoveOperation = new EventEmitter(false);
    this.onChangeOperation = new EventEmitter(false);
  }

  ngOnInit(): void {}
  public remove(operation: Operation) {
    this.onRemoveOperation.emit(operation);
  }
  public change(operation: Operation) {
    this.onChangeOperation.emit(operation);
  }
}
