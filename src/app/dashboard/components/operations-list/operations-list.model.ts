import { Operation } from 'src/app/core/entities';

export interface OperationsDay {
  date: string;
  operations: Operation[];
}
export type OperationsList = Record<string, Operation[]>;
