import { InjectionToken, Provider } from '@angular/core';
import * as moment from 'moment';
// import {
//   MomentDateAdapter,
//   MAT_MOMENT_DATE_ADAPTER_OPTIONS,
// } from '@angular/material-moment-adapter';
// import {
//   DateAdapter,
//   MAT_DATE_FORMATS,
//   MAT_DATE_LOCALE,
// } from '@angular/material/core';
import { combineLatest, map, Observable, of } from 'rxjs';
import { AccountBalance, Operation } from '../core/entities';
import { OperationsList } from './components/operations-list/operations-list.model';
import { DashboardService } from './dashboard.service';

export const OPERATION_LIST = new InjectionToken<Observable<OperationsList>>(
  ''
);
export function operationListFactory(
  dashboardService: DashboardService
): Observable<OperationsList> {
  return dashboardService.operations$.pipe(
    map((data) => {
      const daysMap = new Map<string, Operation[]>();

      data.forEach((operation) => {
        const key: string = moment(operation.date)
          .startOf('day')
          .format('YYYY-MM-DD');

        if (daysMap.has(key)) {
          const values = daysMap.get(key)!;
          values.push(operation);
          const sortedValues = values.sort((a, b) => {
            return moment(b.date).diff(a.date);
          });
          daysMap.set(key, sortedValues);
        } else {
          daysMap.set(key, [operation]);
        }
      });
      // this.dayKeys = [...daysMap.keys()];
      return Object.fromEntries(daysMap);
    })
  );
}
export const ACCOUNT_BALANCE = new InjectionToken<Observable<AccountBalance>>(
  ''
);
export function accountBalanceFactory(
  dashboardService: DashboardService
): Observable<AccountBalance> {
  return dashboardService.operations$.pipe(
    map((data) => {
      const dateNow = moment();
      const operationsInCurrentMonth = data.filter(
        (op) => moment(op.date).month() === dateNow.month()
      );
      const sum = (arr: number[]) => arr.reduce((acc, num) => acc + num, 0);
      const incomeArr = operationsInCurrentMonth
        .filter((op) => op.type === 'income')
        .map((el) => el.amount);
      const income = sum(incomeArr);
      const expenseArr = operationsInCurrentMonth
        .filter((op) => op.type === 'expense')
        .map((el) => el.amount);
      const expense = sum(expenseArr);
      return {
        income,
        accounts: income - expense,
        expense,
        currencyCode: 'RUB',
      };
    })
  );
}

export const DASHBOARD_PROVIDERS: Provider[] = [
  {
    provide: OPERATION_LIST,
    deps: [DashboardService],
    useFactory: operationListFactory,
  },
  {
    provide: ACCOUNT_BALANCE,
    deps: [DashboardService],
    useFactory: accountBalanceFactory,
  },
];
