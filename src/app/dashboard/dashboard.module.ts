import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';
import { CategoryPanelComponent } from './components/category-panel/category-panel.component';
import { SharedModule } from '../shared/shared.module';
import { OperationsListComponent } from './components/operations-list/operations-list.component';
import { DayCardComponent } from './components/operations-list/day-card/day-card.component';
import { OperationCardComponent } from './components/operations-list/operation-card/operation-card.component';

@NgModule({
  declarations: [
    DashboardComponent,
    CategoryPanelComponent,
    OperationsListComponent,
    DayCardComponent,
    OperationCardComponent,
  ],
  imports: [CommonModule, SharedModule, DashboardRoutingModule],
  providers: [DashboardService],
})
export class DashboardModule {}
