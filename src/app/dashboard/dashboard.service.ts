import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { map, Observable } from 'rxjs';
import { Account, ExpCategory, IncCategory, Operation } from '../core/entities';
import { RequestService } from '../core/services/request.service';

@Injectable()
export class DashboardService {
  expCategories$: Observable<ExpCategory[]>;
  incCategories$: Observable<IncCategory[]>;
  accounts$: Observable<Account[]>;
  operations$: Observable<Operation[]>;

  constructor(private readonly requestService: RequestService) {
    this.expCategories$ =
      this.requestService.getCollection<ExpCategory>('expense_categories');
    this.incCategories$ =
      this.requestService.getCollection<IncCategory>('income_categories');
    this.accounts$ = this.requestService.getCollection<Account>('accounts');
    this.operations$ =
      this.requestService.getCollection<Operation>('operations');
  }
  addOperation = (payload: Operation) =>
    this.requestService.add('operations', payload);
  changeOperation = (payload: Operation) =>
    this.requestService.update('operations', payload.id!, payload);
  removeOperation = (payload: Operation) =>
    this.requestService.remove('operations', payload.id!);
}
