import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import {
  Observable,
  map,
  merge,
  concat,
  concatAll,
  concatMap,
  combineLatest,
  filter,
} from 'rxjs';
import { Operation, ExpCategory, AccountBalance } from '../core/entities';
import { AuthService } from '../core/services/auth.service';
import { RequestService } from '../core/services/request.service';
import { OperationUsertModalComponent } from '../shared/components/operation-upsert-modal/operation-usert-modal.component';
import { OperationsList } from './components/operations-list/operations-list.model';
import {
  ACCOUNT_BALANCE,
  DASHBOARD_PROVIDERS,
  OPERATION_LIST,
} from './dashboard.providers';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DASHBOARD_PROVIDERS],
})
export class DashboardComponent implements OnInit, OnDestroy {
  // public user: any;
  public operations$: Observable<OperationsList>;
  public categories$: Observable<any[]>;
  accounts: any;
  incCategories: any;
  expCategories: any;
  public balance$: Observable<AccountBalance>;
  constructor(
    private readonly dialog: MatDialog,
    private readonly authService: AuthService,
    // private readonly requestService: RequestService,
    @Inject(OPERATION_LIST) private operationList$: Observable<OperationsList>,
    @Inject(ACCOUNT_BALANCE)
    private accountBalance$: Observable<AccountBalance>,
    private readonly dashboardService: DashboardService
  ) {
    this.balance$ = this.accountBalance$;
    this.operations$ = this.operationList$;
    this.categories$ = combineLatest([
      this.dashboardService.expCategories$,
      this.dashboardService.incCategories$,
      this.dashboardService.accounts$,
    ]).pipe(
      map(([exp, inc, acc]) => {
        this.accounts = acc;
        this.expCategories = exp;
        this.incCategories = inc;
        return [...exp, ...inc, ...acc];
      })
    );
    // this.user = authService.getCurrentUser();
    // this.categories = this.store
    //   .collection<Category[]>('category')
    //   .valueChanges({ idField: 'id' });
    // this.categories = getObservable(
    //   this.store.collection('category')
    // ) as Observable<ExpCategory[]>;
  }
  public currentUser = () => this.authService.getCurrentUser();
  public ngOnInit(): void {
    // this.getAll();
  }
  public signOut = () => this.authService.signOut();

  // public getAll() {
  //   this.store
  //     .collection('users')
  //     .snapshotChanges()
  //     .subscribe((response) => {
  //       console.log('reponse ', response);
  //     });
  // }
  public onUpsertOperation = (operation?: Operation) => {
    const dialogRef = this.dialog.open(OperationUsertModalComponent, {
      data: {
        operation,
        accounts: this.accounts,
        incCategories: this.incCategories,
        expCategories: this.expCategories,
      },
    });

    dialogRef
      .afterClosed()
      .pipe(filter((res) => res !== null && res !== undefined))
      .subscribe((result: Operation) => {
        // if (!operation) {
        //   this.dashboardService.addOperation(result);
        // }
        console.log(result);

        operation
          ? this.dashboardService.changeOperation({
              ...result,
              id: operation.id,
            })
          : this.dashboardService.addOperation(result);
      });
  };
  public removeOperation(operation: Operation) {
    this.dashboardService.removeOperation(operation);
  }
  public changeCategory(category: ExpCategory) {
    // const dialogRef = this.dialog.open(CategoryUsertModalComponent, {
    //   data: category,
    // });
    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log(`Dialog change result:`, result, category);
    //   this.store.collection('category').doc(category.id).update(result);
    // });
  }
  public onUpsertCategory = () => {
    // const dialogRef = this.dialog.open(CategoryUsertModalComponent);
    // dialogRef.afterClosed().subscribe((result) => {
    //   console.log(`Dialog result:`, result);
    //   this.store.collection('category').add(result);
    // });
  };
  public ngOnDestroy(): void {
    // throw new Error('Method not implemented.');
  }
}
