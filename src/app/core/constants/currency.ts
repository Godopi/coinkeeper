import { Currency } from '../entities';

export const CURRENCY: Currency[] = [
  {
    id: 1,
    name: 'Рубль',
    code: 'RUB',
  },
  {
    id: 2,
    name: 'Доллар',
    code: 'USD',
  },
  {
    id: 3,
    name: 'Евро',
    code: 'EUR',
  },
];
