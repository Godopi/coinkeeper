export interface BaseEntity<TSource> {
  id?: TSource;
  name?: string;
}
