import { Injectable, OnDestroy } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class RequestService implements OnDestroy {
  constructor(private readonly store: AngularFirestore) {}

  public add<T>(colectionName: string, payload: T) {
    return this.store.collection(colectionName).add(payload);
  }
  public update<T>(colectionName: string, docId: string, payload: T) {
    return this.store.collection(colectionName).doc(docId).update(payload);
  }
  public remove(colectionName: string, docId: string) {
    return this.store.collection(colectionName).doc(docId).delete();
  }
  public getCollection<T>(colectionName: string) {
    return this.getObservable<T>(
      this.store.collection(colectionName)
    ) as Observable<T[]>;
  }
  public ngOnDestroy(): void {}

  private getObservable<T>(collection: AngularFirestoreCollection<T>) {
    const subject = new BehaviorSubject<T[]>([]);
    // FIXME отписка или вообще убрать сабскрайб
    collection.valueChanges({ idField: 'id' }).subscribe((val: T[]) => {
      subject.next(val);
    });
    return subject;
  }
}
