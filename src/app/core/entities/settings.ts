export interface Settings {
  period: [string, string];
  currencyId: number;
}
