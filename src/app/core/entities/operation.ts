export interface Operation {
  id?: string;
  fromCategoryId: string;
  toCategoryId: string;
  date: string;
  type: 'transfer' | 'income' | 'expense';
  amount: number;
  currencyCode: string;
  tagId?: number;
  comment?: string;
  repeat?: boolean;
  repeatOptions?: RepeatOptions;
}
export interface RepeatOptions {
  everyDay?: boolean;
  // еще добавить условий
}
