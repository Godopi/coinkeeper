import { BaseEntity } from '../models';

export interface CommonCategory extends BaseEntity<string> {
  budget: number;
  icon?: string;
  deleted: boolean;
}
export interface ExpCategory extends CommonCategory {}
export interface IncCategory extends CommonCategory {}
