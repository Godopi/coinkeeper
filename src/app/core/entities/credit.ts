import { BaseEntity } from '../models';

export interface Credit extends BaseEntity<number> {
  value: number;
  creditOrganizationId: number;
  openingDate: number;
  plannedClosingDate: number;
}
