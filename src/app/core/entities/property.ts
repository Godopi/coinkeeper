import { BaseEntity } from '../models';

export interface Property extends BaseEntity<number> {
  type: number;
  price: number;
}
