// export interface User {
//   id: number;
//   userName: string;
//   // familyName: string;
//   // givenName: string;
//   // middleName: string;

//   // email: string;
//   // emailConfirmed: boolean;
//   // phoneNumber: string;
//   // phoneNumberConfirmed: boolean;
//   // twoFactorEnabled: boolean;
//   // userPermissions: number[];
// }
export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
}
