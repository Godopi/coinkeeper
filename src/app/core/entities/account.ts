import { BaseEntity } from '../models';

export interface Account extends BaseEntity<string> {
  icon?: string;
  deleted: boolean;
  /** карта, кошелек, кредит? */
  typeId: number;
  considerInTotalBalance: boolean;
}
