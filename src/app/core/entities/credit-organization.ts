import { BaseEntity } from '../models';
export enum CreditOrganizationTypes {
    bank = 1,
    MFI = 2,
    person = 3,
    other = 4
}

export interface CreditOrganization extends BaseEntity<number> {
    type: CreditOrganizationTypes;
}
