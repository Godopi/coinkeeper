export interface AccountBalance {
  income: number;
  accounts: number;
  expense: number;
  currencyCode: string;
}
