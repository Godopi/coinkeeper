import { BaseEntity } from '../models';

export interface Currency extends BaseEntity<number> {
  code: string;
}
