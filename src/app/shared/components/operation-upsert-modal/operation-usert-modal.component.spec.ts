import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationUsertModalComponent } from './operation-usert-modal.component';

describe('OperationUsertModalComponent', () => {
  let component: OperationUsertModalComponent;
  let fixture: ComponentFixture<OperationUsertModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OperationUsertModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationUsertModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
