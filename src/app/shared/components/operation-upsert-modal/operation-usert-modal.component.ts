import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import {
  Account,
  ExpCategory,
  IncCategory,
  Operation,
} from 'src/app/core/entities';
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MatDateFormats,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-operation-usert-modal',
  templateUrl: './operation-usert-modal.component.html',
  styleUrls: ['./operation-usert-modal.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class OperationUsertModalComponent implements OnInit {
  public form: FormGroup;
  public types = ['transfer', 'income', 'expense'];
  constructor(
    private dialogRef: MatDialogRef<OperationUsertModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      operation: Operation;
      accounts: Account[];
      incCategories: IncCategory[];
      expCategories: ExpCategory[];
    }
  ) {
    const dateNow = moment();
    this.form = new FormGroup({
      fromCategoryId: new FormControl(
        data.operation && data.operation.fromCategoryId
      ),
      toCategoryId: new FormControl(
        data.operation && data.operation.toCategoryId
      ),
      date: new FormControl(data.operation ? data.operation.date : dateNow),
      amount: new FormControl(data.operation && data.operation.amount),
      comment: new FormControl(data.operation && data.operation.comment),
      type: new FormControl(data.operation && data.operation.type),
      currencyCode: new FormControl('RUB'),
    });
  }

  public ngOnInit(): void {}
  public onSelectFromCategory = (fromCategoryId: string): void => {
    this.form.patchValue({
      fromCategoryId,
    });
  };
  public onSelectToCategory = (toCategoryId: string): void => {
    this.form.patchValue({
      toCategoryId,
    });
  };
  public save = () => {
    this.dialogRef.close({
      ...this.form.value,
      date: this.form.value.date.format('YYYY-MM-DD[T]HH:mm:ss'),
    });
  };

  public close = () => {
    this.dialogRef.close();
  };
}
