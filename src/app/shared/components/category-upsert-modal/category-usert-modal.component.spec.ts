import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryUsertModalComponent } from './category-usert-modal.component';

describe('CategoryUsertModalComponent', () => {
  let component: CategoryUsertModalComponent;
  let fixture: ComponentFixture<CategoryUsertModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryUsertModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryUsertModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
