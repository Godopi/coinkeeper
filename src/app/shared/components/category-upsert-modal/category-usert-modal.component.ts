import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FloatLabelType } from '@angular/material/form-field';
import { expenseIcons, incomeWalletIcons } from 'src/app/core/constants';
import { ExpCategory } from 'src/app/core/entities';

@Component({
  selector: 'app-category-usert-modal',
  templateUrl: './category-usert-modal.component.html',
  styleUrls: ['./category-usert-modal.component.scss'],
})
export class CategoryUsertModalComponent implements OnInit {
  public form: FormGroup;
  public icons = [...expenseIcons, ...incomeWalletIcons];
  constructor(
    private dialogRef: MatDialogRef<CategoryUsertModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ExpCategory | undefined
  ) {
    this.form = new FormGroup({
      // id: new FormControl(),
      name: new FormControl(data && data.name),
      budget: new FormControl(data && data.budget),
      icon: new FormControl(data && data.icon),
      deleted: new FormControl(data ? data.deleted : false),
    });
  }

  public ngOnInit(): void {}
  public onSelectIcon = (iconName: string): void => {
    this.form.patchValue({
      icon: iconName,
    });
  };
  public save = () => {
    this.dialogRef.close(this.form.value);
  };

  public close = () => {
    this.dialogRef.close();
  };
}
