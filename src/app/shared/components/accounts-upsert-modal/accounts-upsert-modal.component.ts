import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { expenseIcons, incomeWalletIcons } from 'src/app/core/constants';
import { Account, ExpCategory } from 'src/app/core/entities';

@Component({
  templateUrl: './accounts-upsert-modal.component.html',
  styleUrls: ['./accounts-upsert-modal.component.scss'],
})
export class AccountsUpsertModalComponent implements OnInit {
  public form: FormGroup;
  public icons = [...expenseIcons, ...incomeWalletIcons];
  constructor(
    private dialogRef: MatDialogRef<AccountsUpsertModalComponent>,
    @Inject(MAT_DIALOG_DATA) public acc: Account | undefined
  ) {
    this.form = new FormGroup({
      // id: new FormControl(),
      name: new FormControl(acc && acc.name),
      icon: new FormControl(acc && acc.icon),
      deleted: new FormControl(acc ? acc.deleted : false),
    });
  }

  public ngOnInit(): void {}
  public onSelectIcon = (iconName: string): void => {
    this.form.patchValue({
      icon: iconName,
    });
  };
  public save = () => {
    this.dialogRef.close(this.form.value);
  };

  public close = () => {
    this.dialogRef.close();
  };
}
