import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountsUpsertModalComponent } from './accounts-upsert-modal.component';

describe('AccountsUpsertModalComponent', () => {
  let component: AccountsUpsertModalComponent;
  let fixture: ComponentFixture<AccountsUpsertModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountsUpsertModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsUpsertModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
