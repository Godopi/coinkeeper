import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeCategoryUpsertModalComponent } from './income-category-upsert-modal.component';

describe('IncomeCategoryUpsertModalComponent', () => {
  let component: IncomeCategoryUpsertModalComponent;
  let fixture: ComponentFixture<IncomeCategoryUpsertModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IncomeCategoryUpsertModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeCategoryUpsertModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
