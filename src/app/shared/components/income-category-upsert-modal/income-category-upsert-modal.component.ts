import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { expenseIcons, incomeWalletIcons } from 'src/app/core/constants';
import { ExpCategory } from 'src/app/core/entities';

@Component({
  templateUrl: './income-category-upsert-modal.component.html',
  styleUrls: ['./income-category-upsert-modal.component.scss'],
})
export class IncomeCategoryUpsertModalComponent implements OnInit {
  public form: FormGroup;
  public icons = [...expenseIcons, ...incomeWalletIcons];
  constructor(
    private dialogRef: MatDialogRef<IncomeCategoryUpsertModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ExpCategory | undefined
  ) {
    this.form = new FormGroup({
      // id: new FormControl(),
      name: new FormControl(data && data.name),
      budget: new FormControl(data && data.budget),
      icon: new FormControl(data && data.icon),
      deleted: new FormControl(data ? data.deleted : false),
    });
  }

  public ngOnInit(): void {}
  public onSelectIcon = (iconName: string): void => {
    this.form.patchValue({
      icon: iconName,
    });
  };
  public save = () => {
    this.dialogRef.close(this.form.value);
  };

  public close = () => {
    this.dialogRef.close();
  };
}
