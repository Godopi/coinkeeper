import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  public form: FormGroup;
  constructor(private readonly authService: AuthService) {
    this.form = new FormGroup({
      email: new FormControl(),
      password: new FormControl(),
    });
  }
  public ngOnInit() {}
  public signUp = () =>
    this.authService.signUp(this.form.value.email, this.form.value.password);
  public googleAuth = () => this.authService.googleAuth();
}
