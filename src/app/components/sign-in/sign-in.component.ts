import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit {
  public form: FormGroup;
  constructor(private readonly authService: AuthService) {
    this.form = new FormGroup({
      email: new FormControl(),
      password: new FormControl(),
    });
  }
  public ngOnInit() {}
  public signIn = () =>
    this.authService.signIn(this.form.value.email, this.form.value.password);
  public googleAuth = () => this.authService.googleAuth();
}
