import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss'],
})
export class VerifyEmailComponent implements OnInit {
  constructor(private readonly authService: AuthService) {}
  public ngOnInit() {}
  public currentUser = () => this.authService.getCurrentUser();
  public sendVerificationMail = () => this.authService.sendVerificationMail();
}
