import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {
  public email: string | null = null;
  constructor(private readonly authService: AuthService) {}
  public ngOnInit() {}
  public forgotPassword = () => {
    this.email && this.authService.forgotPassword(this.email);
  };
}
