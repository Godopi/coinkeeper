// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCYBiFS-DDYJZV2pZ5e0qJsrxvmKcHJ_0U',
    authDomain: 'capital-app-ac791.firebaseapp.com',
    projectId: 'capital-app-ac791',
    storageBucket: 'capital-app-ac791.appspot.com',
    messagingSenderId: '43049970098',
    appId: '1:43049970098:web:4b6ecd7fb2e61ff8779cab'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
